﻿using BrowniePts.Core.Features.Users;
using BrowniePts.Shared.Extensions;
using BrowniePts.Shared.Features.Tokens;
using BrowniePts.Shared.Features.Users;
using BrowniePts.Shared.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BrowniePts.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Account")]
    public class AccountController : BaseController
    {
        private readonly ITokenProvider tokenProvider;
        public AccountController(IMediator mediator, ITokenProvider tokenProvider) : base(mediator)
        {
            this.tokenProvider = tokenProvider;
        }

        [HttpPost]
        [Route("SignUp")]
        public IActionResult SignUp([FromBody]CreateUserCommand command)
        {
            var result = Execute(command);
            if (result.HasErrors)
            {
                return BadRequest(result);
            }
            return result.HasErrors ? (IActionResult) BadRequest(result) : Ok(result);
        }

        [HttpPost]
        [Route("SignIn")]
        public IActionResult SignIn([FromBody]GetTokenQuery query)
        {
            var token = tokenProvider.GetToken(query.EmailAddress, query.Password);
            return token.IsNull() ? (IActionResult)BadRequest("Error retreiving token") : Ok(token);

        }

    }
}