﻿using BrowniePts.Shared.Commands;
using BrowniePts.Shared.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace BrowniePts.WebApi.Controllers
{
    public class BaseController : Controller
    {
        private readonly IMediator mediator;
        public BaseController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        protected new TResult Request<TResult>(IQuery<TResult> query)
        {
            return mediator.Process(query);
        }

        protected BaseCommandResult Execute(ICommand command)
        {
            return mediator.Process(command);
        }

        protected BaseCommandResult<TResult> Execute<TResult>(ICommand<TResult> command)
        {
            return mediator.Process(command);
        }
    }
}