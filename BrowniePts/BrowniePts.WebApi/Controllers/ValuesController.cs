﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using BrowniePts.Shared.Features.Users;
using BrowniePts.Shared.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace BrowniePts.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class ValuesController : BaseController
    {
        public ValuesController(IMediator mediator) : base(mediator)
        {
        }

        // GET api/values
        [HttpGet]
        [HttpPost]
        public IActionResult Get(GetUsersQuery query)
        {
            var users = Request(query);
            return Ok(users);
        }

        [Authorize]
        // GET api/values/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            return Ok($"value: {id}");
        }
    }
}
