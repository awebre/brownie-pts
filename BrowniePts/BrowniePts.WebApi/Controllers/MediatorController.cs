﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading.Tasks;
using BrowniePts.Core;
using BrowniePts.Shared.Commands;
using BrowniePts.Shared.Features.Resolver;
using BrowniePts.Shared.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SimpleInjector;

namespace BrowniePts.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Mediator")]
    public class MediatorController : BaseController
    {
        private List<CachedType> cachedQueries;
        private List<CachedType> cachedCommands;

        private static readonly MethodInfo RequestMI = typeof(IMediator).GetMethod("Request");

        private readonly IMediator mediator;
        private readonly Container container;
        public MediatorController(IMediator mediator, Container container) : base(mediator)
        {
            GetTypeCache();
            this.mediator = mediator;
            this.container = container;
        }

        [HttpPost("Request/{queryName}")]
        public IActionResult ProcessRequest(string queryName, [FromBody]object queryObj)
        {

            var loweredName = queryName.ToLower();

            var queryType = cachedQueries.FirstOrDefault(x => x.Name == loweredName);
            if (queryType == null)
            {
                return BadRequest("Query not found");
            }

            var query = queryObj.DynamicCast(queryType.Type) ?? Activator.CreateInstance(queryType.Type);
            var queryResultType = queryType.Type.GetInterface(typeof(IQuery<>).Name).GetGenericArguments()
                .FirstOrDefault();
            var handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), queryResultType);
            dynamic handler = container.GetInstance(handlerType);
            var result = handler.Execute((dynamic)query);

            return Ok(result);
        }

        [HttpPost("Execute/{commandName}")]
        public IActionResult ProcessCommand(string commandName, [FromBody] object commandObj)
        {
            var loweredName = commandName.ToLower();

            var commandType = cachedCommands.FirstOrDefault(x => x.Name == loweredName);
            if (commandType == null)
            {
                return BadRequest("Command not found");
            }

            var command = commandObj.DynamicCast(commandType.Type) ?? Activator.CreateInstance(commandType.Type);
            var handlerType = typeof(ICommandHandler<>).MakeGenericType(command.GetType(), typeof(void));
            dynamic handler = container.GetInstance(handlerType);
            var result = handler.Execute((dynamic) command);

            return Ok(result);
        }

        private class CachedType
        {
            public string Name { get; set; }
            public Type Type { get; set; }
        }

        private void GetTypeCache()
        {

            var types = AppDomain.CurrentDomain.GetAssemblies().SelectMany(a => a.GetTypes());
            cachedQueries = types.Where(x => x.GetInterface(typeof(IQuery<>).Name) != null).Select(x => new CachedType
            {
                Name = x.Name.ToLower(),
                Type = x
            }).ToList();

            cachedCommands = types.Where(x => x.GetInterface(typeof(ICommand).Name) != null).Select(x => new CachedType
            {
                Name=x.Name.ToLower(),
                Type=x
            }).ToList();
        }
    }
}