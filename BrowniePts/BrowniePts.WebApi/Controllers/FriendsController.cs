﻿using BrowniePts.Shared.Features.Users;
using BrowniePts.Shared.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BrowniePts.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Friends")]
    [Authorize]
    public class FriendsController : BaseController
    {
        public FriendsController(IMediator mediator) : base(mediator)
        {
        }

        public IActionResult PendingFriends(GetPendingFriendsByUserQuery query)
        {
            return Ok(Request(query));
        }

        public IActionResult PendingInvitations(GetPendingInvitationsByUserQuery query)
        {
            return Ok(Request(query));
        }
    }
}