﻿using System;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using BrowniePts.Core.Data;
using BrowniePts.Core.Data.DataSeed;
using SimpleInjector;
using SimpleInjector.Lifestyles;
using Microsoft.AspNetCore.Http;
using AutoMapper;
using Microsoft.AspNetCore.Mvc.Controllers;
using SimpleInjector.Integration.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using BrowniePts.Core.Features;
using BrowniePts.Core.Features.Tokens;
using BrowniePts.Shared.Commands;
using BrowniePts.Shared.Interfaces;
using Microsoft.IdentityModel.Tokens;

namespace BrowniePts.WebApi
{
    public class Startup
    {
        private readonly Container container = new Container();

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            
            //add automapper
            services.AddAutoMapper();

            //add authentication (JWT)
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = "Jwt";
                options.DefaultChallengeScheme = "Jwt";
            }).AddJwtBearer("Jwt", options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidateAudience = false,
                    ValidateIssuer = false,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("br0wn13p01n75f0r7h3w1n")),
                    ValidateLifetime = true,
                    ClockSkew = TimeSpan.FromMinutes(10)
                };
            });

            IntegrateSimpleInjector(services);

            //register data context and initializers
            container.Register(() =>
            {
                var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
                const string connection = @"Server = localhost; Database = BrowniePtsDb; Trusted_Connection = True; ";
                optionsBuilder.UseSqlServer(connection);
                return new DataContext(optionsBuilder.Options);
            }, Lifestyle.Scoped);
            container.Register<UserInitializer>(Lifestyle.Scoped);

            //register queries and queryhandlers dynamically
            container.Register(typeof(IQueryHandler<,>), AppDomain.CurrentDomain.GetAssemblies());

            //register commands and command handlers dynamically
            container.Register(typeof(ICommandHandler<,>), AppDomain.CurrentDomain.GetAssemblies());
            container.Register(typeof(ICommandHandler<>), AppDomain.CurrentDomain.GetAssemblies());

            //register IValidator 
            container.Register(typeof(IValidator<>), AppDomain.CurrentDomain.GetAssemblies());
            container.Register(typeof(FluentValidation.IValidator<>), AppDomain.CurrentDomain.GetAssemblies());

            //get all FluentValidation.IValidators
            var types = container.GetTypesToRegister(typeof(FluentValidation.IValidator<>),
                AppDomain.CurrentDomain.GetAssemblies());
            //fluent validators are those that have a generic interface and are of generic type fluent validator
            //var fluentValidators = types
            //    .SelectMany(x =>
            //        x.GetInterfaces()
            //            .Where(y => y.IsGenericType && y.GetGenericTypeDefinition() == typeof(FluentValidator<>))
            //            .Select(t => t.GetGenericArguments()[0]))
            //    .Select(t => typeof(FluentValidator<>).MakeGenericType(t));

            //container.Register(typeof(IValidator<>), fluentValidators);

            //register decorators
            container.RegisterDecorator(typeof(ICommandHandler<,>), typeof(CommandValidationDecoractor<,>));
            container.RegisterDecorator(typeof(ICommandHandler<>), typeof(CommandValidationDecorator<>));



            //register mediator, token provider
            container.Register<IMediator, Mediator>();
            container.Register<ITokenProvider, TokenProvider>();


            container.Verify();
        }

        private void IntegrateSimpleInjector(IServiceCollection services)
        {
            container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton<IControllerActivator>(
                new SimpleInjectorControllerActivator(container));
            services.AddSingleton<IViewComponentActivator>(
                new SimpleInjectorViewComponentActivator(container));

            services.EnableSimpleInjectorCrossWiring(container);
            services.UseSimpleInjectorAspNetRequestScoping(container);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            using (AsyncScopedLifestyle.BeginScope(container))
            {
                var userInitializer = container.GetInstance<UserInitializer>();
                userInitializer.Seed().Wait();

            }

            app.UseAuthentication();

            app.UseMvc();

        }
    }
}
