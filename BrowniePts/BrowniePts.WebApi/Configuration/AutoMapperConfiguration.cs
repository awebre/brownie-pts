﻿using AutoMapper;
using BrowniePts.Core.Features.Users;
using BrowniePts.Shared.Features.Users;

namespace BrowniePts.WebApi.Configuration
{
    public class AutoMapperConfiguration : Profile
    {
        public AutoMapperConfiguration()
        {
            CreateMap<User, UserDto>().ReverseMap();
        }
    }
}
