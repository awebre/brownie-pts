﻿using BrowniePts.Core.Features.Users;
using BrowniePts.Core.Helpers;
using System.Linq;
using System.Threading.Tasks;
using BrowniePts.Core.Features.Friendships;

namespace BrowniePts.Core.Data.DataSeed
{
    public class UserInitializer
    {
        private readonly DataContext context;

        public UserInitializer(DataContext context)
        {
            this.context = context;
        }

        public async Task Seed()
        {
            context.Database.EnsureCreated();
            var users = context.Set<User>();
            var friendships = context.Set<Friendship>();

            if (!users.Any())
            {
                var admin = new User
                {
                    EmailAddress = "admin@browniepts.com",
                    Password = Crypto.HashPassword("password"),
                };

                var adminSo = new User
                {
                    EmailAddress = "so@browniepts.com",
                    Password = Crypto.HashPassword("password")
                };

                var friendship = new Friendship
                {
                    User = admin,
                    Friend = adminSo,
                    IsSignificantOther = true
                };

                users.Add(admin);
                users.Add(adminSo);

                friendships.Add(friendship);

                await context.SaveChangesAsync();
            }
        }
    }
}
