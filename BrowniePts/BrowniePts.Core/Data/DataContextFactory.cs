﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace BrowniePts.Core.Data
{
    public class DataContextFactory : IDesignTimeDbContextFactory<DataContext>
    {
        public DataContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<DataContext>();
            const string connection = @"Server = localhost; Database = BrowniePtsDb; Trusted_Connection = True; ";
            optionsBuilder.UseSqlServer(connection);
            return new DataContext(optionsBuilder.Options);
        }
    }
}