﻿using BrowniePts.Core.Features.Friendships;
using Microsoft.EntityFrameworkCore;
using BrowniePts.Core.Features.Users;

namespace BrowniePts.Core.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Friendship> Friendships { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //ignore certain properties on User
            modelBuilder.Entity<User>()
                .Ignore(u => u.FullName);
                
            //set foriegn key for Friendship joined table
            modelBuilder.Entity<Friendship>()
                .HasKey(f => new {f.UserId, f.FriendId});

            //set many-to-many relationship for friendship joined table propertiesx
            modelBuilder.Entity<Friendship>()
                .HasOne(f => f.User)
                .WithMany("MyFriends")
                .HasForeignKey(f => f.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Friendship>()
                .HasOne(f => f.Friend)
                .WithMany("FriendsWithMe")
                .HasForeignKey(f => f.FriendId)
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
