﻿using System.ComponentModel.DataAnnotations.Schema;
using BrowniePts.Core.Features.Users;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace BrowniePts.Core.Features.Friendships
{
    public class Friendship
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }

        public int FriendId { get; set; }
        public virtual User Friend { get; set; }

        public bool IsSignificantOther { get; set; }
    }
}