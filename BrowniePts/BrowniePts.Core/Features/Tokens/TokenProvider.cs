﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using BrowniePts.Core.Data;
using BrowniePts.Core.Features.Users;
using BrowniePts.Core.Helpers;
using BrowniePts.Shared.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;

namespace BrowniePts.Core.Features.Tokens
{
    public class TokenProvider : ITokenProvider
    {
        private readonly DbSet<User> users;
        private readonly IQueryHandler<GetUserByEmailQuery, User> getUserQueryHandler;
        public TokenProvider(DataContext context, IQueryHandler<GetUserByEmailQuery, User> getUserQueryHandler)
        {
            users = context.Users;
            this.getUserQueryHandler = getUserQueryHandler;
        }
        public string GetToken(string email, string password)
        {
            var user = getUserQueryHandler.Execute(new GetUserByEmailQuery() {EmailAddress = email});
            if (user == null)
            {
                return null;
            }

            var isValidPassword = Crypto.VerifyHashedPassword(user.Password, password);
            if (!isValidPassword)
            {
                return null;
            }

            var claims = new[]
            {
                new Claim(ClaimTypes.Name, email),
                new Claim(JwtRegisteredClaimNames.Nbf, new DateTimeOffset(DateTime.Now).ToUnixTimeSeconds().ToString()),
                new Claim(JwtRegisteredClaimNames.Exp, new DateTimeOffset(DateTime.Now.AddDays(30)).ToUnixTimeSeconds().ToString()),
            };

            //TODO:
            //The appsecret should be available via some settings constant
            //Look into injecting an IConfiguration instance into this query
            var symmetricKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("br0wn13p01n75f0r7h3w1n"));
            var signingCreds = new SigningCredentials(symmetricKey, SecurityAlgorithms.HmacSha256);
            var jwtHeader = new JwtHeader(signingCreds);
            var jwtPayload = new JwtPayload(claims);
            return new JwtSecurityTokenHandler().WriteToken(new JwtSecurityToken(jwtHeader, jwtPayload));
        }
    }
}