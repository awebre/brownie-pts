﻿using BrowniePts.Shared.Commands;
using SimpleInjector;
using BrowniePts.Shared.Interfaces;

namespace BrowniePts.Core.Features
{
    public class Mediator : IMediator
    {
        private readonly Container container;
        public Mediator(Container container)
        {
            this.container = container;
        }

        public TResult Process<TResult>(IQuery<TResult> query)
        {
            var handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult));
            dynamic handler = container.GetInstance(handlerType);
            return handler.Execute((dynamic)query);
        }

        public BaseCommandResult Process(ICommand command)
        {
            var handlerType = typeof(ICommandHandler<>).MakeGenericType(command.GetType());
            dynamic handler = container.GetInstance(handlerType);
            return handler.Execute((dynamic)command);
        }

        public BaseCommandResult<TResult> Process<TResult>(ICommand<TResult> command)
        {
            var handlerType = typeof(ICommandHandler<,>).MakeGenericType(command.GetType(), typeof(TResult));
            dynamic handler = container.GetInstance(handlerType);
            return handler.Execute((dynamic) command);
        }
    }
}
