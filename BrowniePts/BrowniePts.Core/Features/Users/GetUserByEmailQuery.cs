﻿using System.Linq;
using BrowniePts.Core.Data;
using BrowniePts.Shared.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BrowniePts.Core.Features.Users
{
    public class GetUserByEmailQuery : IQuery<User>
    {
        public string EmailAddress { get; set; }
    }

    public class GetUserByEmailQueryHandler : IQueryHandler<GetUserByEmailQuery, User>
    {
        private readonly DbSet<User> users;
        public GetUserByEmailQueryHandler(DataContext context)
        {
            this.users = context.Users;
        }
        public User Execute(GetUserByEmailQuery query)
        {
            return users.FirstOrDefault(u => u.EmailAddress == query.EmailAddress);
        }
    }
}