﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using BrowniePts.Core.Data;
using BrowniePts.Shared.Features.Users;
using BrowniePts.Shared.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BrowniePts.Core.Features.Users
{
    public class GetUsersQueryHandler : IQueryHandler<GetUsersQuery, List<UserDto>>
    {
        private readonly DbSet<User> users;
        public GetUsersQueryHandler(DataContext context)
        {
            this.users = context.Users;
        }
        public List<UserDto> Execute(GetUsersQuery query)
        {
            return Mapper.Map<List<User>, List<UserDto>>(users.ToList());
        }
    }
}
