﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BrowniePts.Core.Data;
using BrowniePts.Shared.Features.Users;
using BrowniePts.Shared.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BrowniePts.Core.Features.Users
{
    public class GetPendingFriendsByUserQueryHandler : IQueryHandler<GetPendingFriendsByUserQuery, List<UserDto>>
    {
        private readonly DbSet<User> users;
        public GetPendingFriendsByUserQueryHandler(DataContext context)
        {
            users = context.Users;
        }

        public List<UserDto> Execute(GetPendingFriendsByUserQuery query)
        {
            var user = users.Include(x => x.MyFriends).SingleOrDefault(x => x.EmailAddress == query.EmailAddress);
            var pendingFriendships = user?.MyFriends.Where(m => user.FriendsWithMe.All(f => f.User != m.Friend));
            var pendingFriends = new List<UserDto>();
            if (pendingFriendships == null) return pendingFriends;
            pendingFriends.AddRange(pendingFriendships.Select(pendingFriendship => Mapper.Map<UserDto>(pendingFriendship.Friend)));
            return pendingFriends;
        }
    }
}