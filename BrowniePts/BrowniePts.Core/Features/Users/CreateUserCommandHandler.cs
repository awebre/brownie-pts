﻿using System.Linq;
using System.Net.Mail;
using AutoMapper;
using BrowniePts.Core.Data;
using BrowniePts.Core.Helpers;
using BrowniePts.Shared.Commands;
using BrowniePts.Shared.Extensions;
using BrowniePts.Shared.Features.Users;
using BrowniePts.Shared.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace BrowniePts.Core.Features.Users
{
    public class CreateUserCommandHandler : ICommandHandler<CreateUserCommand, UserDto>
    {
        private readonly DataContext context;
        public CreateUserCommandHandler(DataContext context)
        {
            this.context = context;
        }

        public BaseCommandResult<UserDto> Execute(CreateUserCommand command)
        {
            var newUser = new User()
            {
                FirstName = command.FirstName,
                LastName = command.LastName,
                EmailAddress = command.EmailAddress,
                Password = Crypto.HashPassword(command.Password)
            };

            context.Users.Add(newUser);
            context.SaveChanges();
            var userDto = Mapper.Map<UserDto>(newUser);
            return new BaseCommandResult().ConvertTo(userDto);
        }
    }

    public class CreateUserCommandValidator : AbstractValidator<CreateUserCommand>
    {
        private readonly DbSet<User> users;
        public CreateUserCommandValidator(DataContext dataContext)
        {
            this.users = dataContext.Users;
            RuleFor(x => x.Password)
                .NotEmpty()
                .NotNull()
                .WithMessage("Password is required")
                .Must((x, y) => MatchConfirmPassword(y, x.ConfirmPassword))
                .WithMessage("Password and confirm password do not match.");
            RuleFor(x => x.EmailAddress)
                .NotEmpty()
                .NotNull()
                .WithMessage("Email Address is required")
                .Must(BeAValidEmailAddress)
                .WithMessage("The email address you entered was invalid")
                .Must(BeAUniqueEmail)
                .WithMessage("There is already an account with that email address.");
        }

        private static bool MatchConfirmPassword(string password, string confirmPassword)
        {
            if (password.IsNull() || confirmPassword.IsNull())
                return false;
            return password.Equals(confirmPassword);
        }

        private static bool BeAValidEmailAddress(string emailAddress)
        {
            try
            {
                var address = new MailAddress(emailAddress);
                return address.Address == emailAddress;
            }
            catch
            {
                return false;
            }
        }

        private bool BeAUniqueEmail(string emailAddress)
        {
            return users.Any(u => u.EmailAddress == emailAddress);
        }
    }
}
