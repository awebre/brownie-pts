﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using BrowniePts.Core.Data;
using BrowniePts.Shared.Features.Users;
using BrowniePts.Shared.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace BrowniePts.Core.Features.Users
{
    public class GetPendingInvitationsByUserQueryHandler : IQueryHandler<GetPendingInvitationsByUserQuery, List<UserDto>>
    {
        private readonly DbSet<User> users;
        public GetPendingInvitationsByUserQueryHandler(DataContext context)
        {
            users = context.Users;
        }

        public List<UserDto> Execute(GetPendingInvitationsByUserQuery query)
        {
            var user = users.Include(x => x.MyFriends).SingleOrDefault(x => x.EmailAddress == query.EmailAddress);
            var pendingInvitations = user?.FriendsWithMe.Where(m => user.MyFriends.All(f => f.User != m.Friend));
            var pendingFriends = new List<UserDto>();
            if (pendingInvitations == null) return pendingFriends;
            pendingFriends.AddRange(pendingInvitations.Select(pendingFriendship => Mapper.Map<UserDto>(pendingFriendship.Friend)));
            return pendingFriends;
        }
    }
}