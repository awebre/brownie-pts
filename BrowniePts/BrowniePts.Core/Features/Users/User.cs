﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using BrowniePts.Core.Features.Friendships;

namespace BrowniePts.Core.Features.Users
{
    public class User
    {
        private string firstName = string.Empty;
        private string lastName = string.Empty;
        public int Id { get; set; }

        public string EmailAddress { get; set; }

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value ?? string.Empty; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value ?? string.Empty; }
        }

        public string FullName => $"{FirstName} {LastName}";

        public string Password { get; set; }

        public ICollection<Friendship> MyFriends { get; set; }

        public ICollection<Friendship> FriendsWithMe { get; set; }
    }
}
