﻿using System;
using System.Reflection;

namespace BrowniePts.Core
{
    public static class CastExtensions
    {
        public static dynamic DynamicCast(this object obj, Type castTo)
        {
            var castMethodInfo = typeof(CastExtensions).GetMethod("Cast").MakeGenericMethod(castTo);
            return castMethodInfo.Invoke(null, new object[] { obj });
        }
        public static T Cast<T>(object obj) where T : class 
        {
            return obj as T;
        }
    }
}