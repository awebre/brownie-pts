﻿using System;
using SimpleInjector;

namespace BrowniePts.Shared.Features.Resolver
{
    public class Resolver
    {
        private readonly Container container;

        public Resolver(Container container)
        {
            this.container = container;
        }

        public T Resolve<T>() where T : class
        {
            return container.GetInstance<T>();
        }
    }
}
