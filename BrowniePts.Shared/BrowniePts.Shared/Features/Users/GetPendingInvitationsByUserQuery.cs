﻿using System.Collections.Generic;
using BrowniePts.Shared.Interfaces;

namespace BrowniePts.Shared.Features.Users
{
    public class GetPendingInvitationsByUserQuery : IQuery<List<UserDto>>
    {
        public string EmailAddress { get; set; }
    }
}