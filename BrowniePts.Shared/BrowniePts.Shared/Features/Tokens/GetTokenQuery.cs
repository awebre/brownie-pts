﻿using BrowniePts.Shared.Interfaces;

namespace BrowniePts.Shared.Features.Tokens
{
    public class GetTokenQuery : IQuery<string>
    {
        public string EmailAddress { get; set; }

        public string Password { get; set; }
    }
}