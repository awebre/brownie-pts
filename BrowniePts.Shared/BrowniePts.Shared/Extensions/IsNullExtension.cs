﻿namespace BrowniePts.Shared.Extensions
{
    public static class IsNullExtension
    {
        public static bool IsNull<T>(this T obj) where T : class
        {
            return obj == null;
        }

        public static bool IsNull<T>(this T? nullableObj) where T : struct
        {
            return !nullableObj.HasValue;
        }
    }
}