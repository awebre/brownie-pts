﻿using System.Collections.Generic;

namespace BrowniePts.Shared.Commands
{
    public interface IValidationResult
    {
        bool IsValid { get; }

        IEnumerable<ValidationError> Errors { get; }
    }
}