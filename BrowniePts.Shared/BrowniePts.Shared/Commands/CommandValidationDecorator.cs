﻿namespace BrowniePts.Shared.Commands
{
    public class CommandValidationDecorator<TCommand> : ICommandHandler<TCommand> where TCommand : ICommand
    {
        private readonly ICommandHandler<TCommand> commandHandler;
        private readonly FluentValidation.IValidator<TCommand> validator;

        public CommandValidationDecorator(ICommandHandler<TCommand> commandHandler, FluentValidation.IValidator<TCommand> validator)
        {
            this.commandHandler = commandHandler;
            this.validator = validator;
        }

        public BaseCommandResult Execute(TCommand command)
        {
            var result = validator.Validate(command);
            if (result != null && !result.IsValid)
            { 
                var commandResult = new BaseCommandResult();
                foreach (var validationFailure in result.Errors)
                {
                    var validationError = new ValidationError(validationFailure.PropertyName,
                        validationFailure.ErrorMessage, validationFailure.AttemptedValue);
                    commandResult.Errors.Add(validationError);
                }
                return commandResult;
            }
            return commandHandler.Execute(command);
        }
    }

    public class CommandValidationDecoractor<TCommand, TResult> : ICommandHandler<TCommand, TResult>
        where TCommand : ICommand<TResult>
    {
        private readonly ICommandHandler<TCommand, TResult> commandHandler;
        private readonly FluentValidation.IValidator<TCommand> validator;

        public CommandValidationDecoractor(ICommandHandler<TCommand, TResult> commandHandler,
            FluentValidation.IValidator<TCommand> validator)
        {
            this.commandHandler = commandHandler;
            this.validator = validator;
        }

        public BaseCommandResult<TResult> Execute(TCommand command)
        {
            var result = validator.Validate(command);
            if (result != null && !result.IsValid)
            {
                var commandResult = new BaseCommandResult<TResult>();
                foreach (var validationFailure in result.Errors)
                {
                    var validationError = new ValidationError(validationFailure.PropertyName,
                        validationFailure.ErrorMessage, validationFailure.AttemptedValue);
                    commandResult.Errors.Add(validationError);
                }
                return commandResult;
            }
            return commandHandler.Execute(command);
        }
    }

    public class FluentValidator<T> : IValidator<T>
    {
        private readonly FluentValidation.IValidator<T> fluentValidator;

        public FluentValidator(FluentValidation.IValidator<T> fluentValidator)
        {
            this.fluentValidator = fluentValidator;
        }

        public IValidationResult Validate(T command)
        {
            return new FluentValidationResult(fluentValidator.Validate(command));
        }
    }
}
