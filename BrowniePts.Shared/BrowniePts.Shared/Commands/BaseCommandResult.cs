﻿using System.Collections.Generic;
using System.Linq;

namespace BrowniePts.Shared.Commands
{
    public class BaseCommandResult
    {
        private ICollection<ValidationError> errors = new List<ValidationError>();

        public ICollection<ValidationError> Errors
        {
            get => errors;
            set => errors = value ?? new List<ValidationError>();
        }

        public bool HasErrors => Errors.Any();

        public BaseCommandResult<TResult> ConvertTo<TResult>()
        {
            return new BaseCommandResult<TResult>
            {
                Errors = Errors
            };
        }

        public BaseCommandResult<TResult> ConvertTo<TResult>(TResult result)
        {
            return new BaseCommandResult<TResult>
            {
                Errors = Errors,
                Result = result
            };
        }
    }

    public class BaseCommandResult<T> : BaseCommandResult
    {
        public T Result { get; set; }
    }
}