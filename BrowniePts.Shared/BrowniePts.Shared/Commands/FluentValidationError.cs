﻿namespace BrowniePts.Shared.Commands
{
    public class FluentValidationError : ValidationError
    {
        public FluentValidationError(string propertyName, string errorMessage, object attemptedValue)
        {
            PropertyName = propertyName;
            ErrorMessage = errorMessage;
            AttemptedValue = attemptedValue;
        }
    }
}