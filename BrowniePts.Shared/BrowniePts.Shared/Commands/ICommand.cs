﻿namespace BrowniePts.Shared.Commands
{
    public interface ICommand
    {
    }

    public interface ICommand<TResult>
    {
    }

    public interface ICommandHandler<in TCommand> where TCommand : ICommand
    {
        BaseCommandResult Execute(TCommand command);
    }

    public interface ICommandHandler<in TCommand, TResult> where TCommand : ICommand<TResult>
    {
        BaseCommandResult<TResult> Execute(TCommand command);
    }
}
