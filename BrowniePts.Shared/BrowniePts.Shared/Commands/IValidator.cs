﻿namespace BrowniePts.Shared.Commands
{
    public interface IValidator<in TCommand>
    {
        IValidationResult Validate(TCommand command);
    }
}