﻿namespace BrowniePts.Shared.Commands
{
    public class ValidationError
    {
        protected ValidationError()
        {
        }

        public ValidationError(string propertyName, string errorMessage, object attemptedValue)
        {
            PropertyName = propertyName;
            ErrorMessage = errorMessage;
            AttemptedValue = attemptedValue;
        }


        public string PropertyName { get; set; }
        public string ErrorMessage { get; set; }
        public object AttemptedValue { get; set; } 
    }
}