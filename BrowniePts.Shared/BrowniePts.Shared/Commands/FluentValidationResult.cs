﻿using System.Collections.Generic;
using FluentValidation.Results;

namespace BrowniePts.Shared.Commands
{
    public class FluentValidationResult : IValidationResult
    {
        private readonly ValidationResult validationResult;
        private readonly List<ValidationError> errors = new List<ValidationError>();
        public FluentValidationResult(ValidationResult validationResult)
        {
            this.validationResult = validationResult;
            foreach (var validationError in validationResult.Errors)
            {
                this.errors.Add(new FluentValidationError(validationError.PropertyName, validationError.ErrorMessage,
                    validationError.AttemptedValue));
            }
        }

        public bool IsValid => validationResult.IsValid;
        public IEnumerable<ValidationError> Errors => errors.AsReadOnly();
    }
}