﻿namespace BrowniePts.Shared.Interfaces
{
    public interface IQuery<TResult>
    {
    }

    public interface IQueryHandler<in TQuery, out TResult>
    {
        TResult Execute (TQuery query);
    }
}
