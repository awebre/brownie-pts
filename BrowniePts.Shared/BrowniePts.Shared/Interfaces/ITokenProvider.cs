﻿namespace BrowniePts.Shared.Interfaces
{
    public interface ITokenProvider
    {
        string GetToken(string email, string password);
    }
}