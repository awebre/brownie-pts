﻿using BrowniePts.Shared.Commands;

namespace BrowniePts.Shared.Interfaces
{
    public interface IMediator
    {
        TResult Process<TResult>(IQuery<TResult> query);
        BaseCommandResult Process(ICommand command);
        BaseCommandResult<TResult> Process<TResult>(ICommand<TResult> command);
    }
}
