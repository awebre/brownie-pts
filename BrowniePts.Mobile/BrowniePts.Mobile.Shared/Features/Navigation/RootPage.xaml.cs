﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using BrowniePts.Mobile.Shared.Configuration;
using BrowniePts.Mobile.Shared.Features.Account;
using BrowniePts.Mobile.Shared.Features.Network;
using BrowniePts.Mobile.Shared.PlatformServices;
using BrowniePts.Shared.Features.Resolver;
using BrowniePts.Shared.Features.Users;
using Xamarin.Forms;

namespace BrowniePts.Mobile.Shared.Features.Navigation
{
    public partial class RootPage : ContentPage
    {
        private readonly Resolver resolver;
        private readonly IKeyValueStorageService storageService;
        private readonly ApiService apiService;
        public RootPage(Resolver resolver, IKeyValueStorageService storageService, ApiService apiService)
        {
            this.resolver = resolver;
            this.storageService = storageService;
            this.apiService = apiService;

            NavigateBasedOnKey();
            InitializeComponent();
            var Button = this.FindByName<Button>("button");
            Button.Clicked += async (sender, e) =>
            {
                await apiService.MakeRequest<string>("values/5", null, response =>
                {
                    Debug.WriteLine(response);
                });
            };

            var LogOutButton = this.FindByName<Button>("logout");
            LogOutButton.Clicked += (sender, e) => {
                storageService.Delete(AppSettings.TokenStorageKey);
                NavigateBasedOnKey();
            };
        }

        protected override void OnAppearing()
        {
            NavigateBasedOnKey();
        }

        async void NavigateBasedOnKey(){
            var token = storageService.GetByKey(AppSettings.TokenStorageKey);
            if(token == null){
                var loginPage = resolver.Resolve<LoginPage>();
                await Navigation.PushModalAsync(loginPage);
            }
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            var query = new GetUsersQuery();
            await apiService.MakeRequest<List<UserDto>>("/Mediator/Request/GetUsersQuery", query, response => {
                Debug.WriteLine(response);  
            });
        }

        async void Handle_FriendsClicked(object sender, System.EventArgs e)
        {
            var friendPage = resolver.Resolve<FriendsPage>();
            await Navigation.PushAsync(friendPage);
        }
    }
}
