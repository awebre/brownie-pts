﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using BrowniePts.Mobile.Shared.Configuration;
using BrowniePts.Mobile.Shared.PlatformServices;
using BrowniePts.Shared.Commands;
using Newtonsoft.Json;

namespace BrowniePts.Mobile.Shared.Features.Network
{
    public class ApiService
    {
        private readonly HttpClient client;
        private readonly string apiUrl;
        private readonly IKeyValueStorageService storageService;
        private readonly IDialogProvider dialogProvider;
        public ApiService(string apiUrl, IKeyValueStorageService storageService, IDialogProvider dialogProvider)
        {
            client = new HttpClient();
            this.apiUrl = apiUrl;
            this.storageService = storageService;
            this.dialogProvider = dialogProvider;

        }

        public async Task MakeRequest<TResult>(string endpoint, object data = null, Action<TResult> successAction = null)
        {
            var token = storageService.GetByKey(AppSettings.TokenStorageKey);
            if(token != null){
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            }
            var uri = new Uri($"{apiUrl}/{endpoint}");
            HttpResponseMessage response = null;
            if (data != null)
            {
                var json = JsonConvert.SerializeObject(data);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                response = client.PostAsync(uri.ToString(), content).Result;
            }
            else
            {
                response = client.GetAsync(uri).Result;
            }

            var responseContent = await response.Content.ReadAsStringAsync();

            if (response.IsSuccessStatusCode)
            {
                successAction?.Invoke(JsonConvert.DeserializeObject<TResult>(responseContent));
            }

            if(response.StatusCode == HttpStatusCode.BadRequest)
            {
                var validationResult = JsonConvert.DeserializeObject<BaseCommandResult>(responseContent);
                if(validationResult != null)
                {
                    var message = "The request had the following errors: ";
                    foreach(var error in validationResult.Errors)
                    {
                        message += $"\n - {error.ErrorMessage}";
                    }
                    dialogProvider.ShowMessage("Request Error", message, "Dismiss", null);
                }
            }

            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                storageService.Delete(AppSettings.TokenStorageKey);    
            }

        }
    }
}