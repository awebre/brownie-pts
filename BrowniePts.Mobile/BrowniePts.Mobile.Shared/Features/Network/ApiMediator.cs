﻿using System;
using System.Threading.Tasks;
using BrowniePts.Shared.Commands;
using BrowniePts.Shared.Interfaces;

namespace BrowniePts.Mobile.Shared.Features.Network
{
    public class ApiMediator : IApiMediator
    {
        private readonly ApiService apiService;
        public ApiMediator(ApiService apiService)
        {
            this.apiService = apiService;
        }

        public async Task Process(string endpoint, ICommand command, Action successAction = null)
        {
            Action<BaseCommandResult> action = null;
            if(successAction != null)
            {
                action = result => successAction();
            }
            await apiService.MakeRequest(endpoint, command, action);
        }

        public async Task Process<TResult>(string endpoint, ICommand<TResult> command, Action<TResult> successAction)
        {
            Action<BaseCommandResult<TResult>> action = null;
            if(successAction != null)
            {
                action = result => successAction(result.Result);    
            }
            await apiService.MakeRequest(endpoint, command, successAction);
        }

        public async Task Process<TResult>(string endpoint, IQuery<TResult> query, Action<TResult> successAction)
        {
            await apiService.MakeRequest(endpoint, query, successAction);
        }
    }
}
