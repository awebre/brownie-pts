﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace BrowniePts.Mobile.Shared.Features.Account
{
    public partial class LoginPage : ContentPage
    {
        private LoginPageViewModel viewModel;

        public LoginPage(LoginPageViewModel viewModel)
        {
            this.viewModel = viewModel;
            InitializeComponent();

            BindingContext = viewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            viewModel.OnAppearing();
        }
    }
}
