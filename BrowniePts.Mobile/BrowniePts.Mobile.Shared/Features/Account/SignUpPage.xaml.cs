﻿using System;
using System.Collections.Generic;
using BrowniePts.Mobile.Shared.Configuration;
using BrowniePts.Mobile.Shared.PlatformServices;
using BrowniePts.Shared.Features.Resolver;
using Xamarin.Forms;

namespace BrowniePts.Mobile.Shared.Features.Account
{
    public partial class SignUpPage : ContentPage
    {
        private SignUpPageViewModel viewModel;
        private readonly Resolver resolver;
        public SignUpPage(Resolver resolver)
        {
            this.resolver = resolver;
            InitializeComponent();
            viewModel = resolver.Resolve<SignUpPageViewModel>();
            BindingContext = viewModel;
        }

        protected override void OnAppearing()
        {
            NavigateBasedOnKey();
        }

        async void NavigateBasedOnKey()
        {
            var token = resolver.Resolve<IKeyValueStorageService>().GetByKey(AppSettings.TokenStorageKey);
            if (token != null)
            {
                await Navigation.PopModalAsync();
            }
        }
    }
}
