﻿using System.Collections.Generic;
using System.Reflection;
using BrowniePts.Mobile.Shared.Features.Navigation;
using BrowniePts.Mobile.Shared.Features.Network;
using BrowniePts.Shared.Features.Resolver;
using BrowniePts.Mobile.Shared.Configuration;
using SimpleInjector;
using Xamarin.Forms;
using BrowniePts.Mobile.Shared.PlatformServices;

namespace BrowniePts.Mobile.Shared
{
    public partial class App : Application
    {
        //public App()
        //{
        //    InitializeComponent();

        //    MainPage = new RootPage();
        //}

        public App(Container container, IEnumerable<Assembly> Assemblies)
        {
            Container = container;

            container.Register<Resolver>();
            container.Register(() => new ApiService(AppSettings.ApiUrl, container.GetInstance<IKeyValueStorageService>(), container.GetInstance<IDialogProvider>()));


            var resolver = container.GetInstance<Resolver>();

            MainPage = new NavigationPage(resolver.Resolve<RootPage>());
        }

        public static Container Container { get; set; }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
