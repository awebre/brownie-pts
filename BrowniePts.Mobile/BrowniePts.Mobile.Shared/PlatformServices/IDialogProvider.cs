﻿using System;
namespace BrowniePts.Mobile.Shared.PlatformServices
{
    public interface IDialogProvider
    {
        void ShowMessage(string title, string message, string dismissTitle, Action dismiss);
        void ConfirmMessage(string title, string message, string confirmTitle, string dismissTitle, Action confirm , Action dismiss);
    }
}
