﻿using System;
namespace BrowniePts.Mobile.Shared.PlatformServices
{
    public interface IKeyValueStorageService
    {
        string GetByKey(string key);
        void Store(string key, string value);
        void Delete(string key);
    }
}
