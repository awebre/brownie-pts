﻿using System;
using Android.App;
using BrowniePts.Mobile.Shared.PlatformServices;
using Xamarin.Forms;

namespace BrowniePts.Mobile.Droid.PlatformServices
{
    public class DialogProvider : IDialogProvider
    {
        public void ConfirmMessage(string title, string message, string confirmTitle, string dismissTitle, Action confirm, Action dismiss)
        {
            var builder = new AlertDialog.Builder(Forms.Context as Activity);
            var dialog = builder.Create();
            builder.SetTitle(title);
            builder.SetMessage(message);
            builder.SetNeutralButton(confirmTitle, (sender, e) => confirm?.Invoke());
            builder.SetNegativeButton(dismissTitle, (sender, e) => confirm?.Invoke());
            builder.Show();
        }

        public void ShowMessage(string title, string message, string dismissTitle, Action dismiss)
        {
            var builder = new AlertDialog.Builder(Forms.Context as Activity);
            var dialog = builder.Create();
            builder.SetTitle(title);
            builder.SetMessage(message);
            builder.SetNegativeButton(dismissTitle, (sender, e) => dismiss?.Invoke());
            builder.Show();
        }
    }
}
