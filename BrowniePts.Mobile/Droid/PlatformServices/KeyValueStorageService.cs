﻿using System;
using Android.App;
using Android.Content;
using Android.Preferences;
using BrowniePts.Mobile.Shared.PlatformServices;

namespace BrowniePts.Mobile.Droid.PlatformServices
{
    public class KeyValueStorageService : IKeyValueStorageService
    {
        private ISharedPreferences SharedPreferenceManager => PreferenceManager.GetDefaultSharedPreferences(Application.Context);

        public void Delete(string key)
        {
            var editor = SharedPreferenceManager.Edit();
            editor.Remove(key);
            editor.Apply();
        }

        public string GetByKey(string key)
        {
            return SharedPreferenceManager.GetString(key, string.Empty);
        }

        public void Store(string key, string value)
        {
            var editor = SharedPreferenceManager.Edit();
            editor.PutString(key, value);
            editor.Apply();
        }
    }
}
