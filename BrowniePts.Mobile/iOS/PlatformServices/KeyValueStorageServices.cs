﻿using System;
using BrowniePts.Mobile.Shared.PlatformServices;
using Foundation;
using Security;

namespace BrowniePts.Mobile.iOS.PlatformServices
{
    public class KeyValueStorageService : IKeyValueStorageService
    {
        public void Delete(string key)
        {
            var query = new SecRecord(SecKind.GenericPassword)
            {
                Service = NSBundle.MainBundle.BundleIdentifier,
                Account = key
            };
            SecKeyChain.Remove(query);
        }

        public string GetByKey(string key)
        {
            var query = new SecRecord(SecKind.GenericPassword)
            {
                Service = NSBundle.MainBundle.BundleIdentifier,
                Account = key
            };
            SecStatusCode result;
            var record = SecKeyChain.QueryAsRecord(query, out result);

            if(record == null)
            {
                return null;
            }

            var serializedData = NSString.FromData(record.Generic, NSStringEncoding.UTF8);
            return serializedData;
        }

        public void Store(string key, string value)
        {
            Delete(key);
            var record = new SecRecord(SecKind.GenericPassword)
            {
                Service = NSBundle.MainBundle.BundleIdentifier,
                Account = key,
                Generic = NSData.FromString(value, NSStringEncoding.UTF8),
                Accessible = SecAccessible.WhenUnlocked
            };
            SecKeyChain.Add(record);
        }
    }
}
