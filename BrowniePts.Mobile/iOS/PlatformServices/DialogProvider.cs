﻿using System;
using BrowniePts.Mobile.Shared.PlatformServices;
using UIKit;

namespace BrowniePts.Mobile.iOS.PlatformServices
{
    public class DialogProvider : IDialogProvider
    {

        public void ConfirmMessage(string title, string message, string confirmTitle, string dismissTitle, Action confirm, Action dismiss)
        {
            var topViewController = GetMostPresentedViewController();
            var alertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);
            alertController.AddAction(UIAlertAction.Create(confirmTitle, UIAlertActionStyle.Default, alert => confirm?.Invoke()));
            alertController.AddAction(UIAlertAction.Create(dismissTitle, UIAlertActionStyle.Default, alert => dismiss?.Invoke()));
            topViewController.PresentViewController(alertController, true, null);
        }

        public void ShowMessage(string title, string message, string dismissTitle, Action dismiss)
        {
            var topViewController = GetMostPresentedViewController();
            var alertController = UIAlertController.Create(title, message, UIAlertControllerStyle.Alert);
            alertController.AddAction(UIAlertAction.Create(dismissTitle, UIAlertActionStyle.Default, alert => dismiss?.Invoke()));
            topViewController.PresentViewController(alertController, true, null);
        }

        private UIViewController GetMostPresentedViewController()
        {
            var viewController = UIApplication.SharedApplication.KeyWindow.RootViewController;
            while(viewController.PresentedViewController != null)
            {
                viewController = viewController.PresentedViewController;
            }
            return viewController;
        }
    }
}
