﻿using System;
using System.Collections.Generic;
using System.Linq;
using BrowniePts.Mobile.iOS.Configuration;
using BrowniePts.Mobile.Shared;
using Foundation;
using UIKit;

namespace BrowniePts.Mobile.iOS
{
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();

            var container = SimpleInjectorConfiguration.Configure();


            LoadApplication(new App(container, AppDomain.CurrentDomain.GetAssemblies()));

            return base.FinishedLaunching(app, options);
        }
    }
}
