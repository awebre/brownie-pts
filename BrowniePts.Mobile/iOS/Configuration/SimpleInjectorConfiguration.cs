﻿using System;
using BrowniePts.Mobile.iOS.PlatformServices;
using BrowniePts.Mobile.Shared.PlatformServices;
using SimpleInjector;

namespace BrowniePts.Mobile.iOS.Configuration
{
    public static class SimpleInjectorConfiguration
    {
        public static Container Configure(){
            var container = new Container();

            container.Register<IKeyValueStorageService, KeyValueStorageService>();
            container.Register<IDialogProvider, DialogProvider>();

            return container;
        }
    }
}
