﻿using System;
namespace BrowniePts.Mobile.PlatformServices
{
    public interface IKeyValueStorageService
    {
        string GetByKey(string key);
        void Store(string key, string value);
        void Delete(string key);
    }
}
