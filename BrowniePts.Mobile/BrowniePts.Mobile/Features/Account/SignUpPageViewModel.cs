﻿using System;
using System.Diagnostics;
using System.Windows.Input;
using BrowniePts.Mobile.Configuration;
using BrowniePts.Mobile.Features.Network;
using BrowniePts.Mobile.PlatformServices;
using BrowniePts.Shared.Features.Resolver;
using BrowniePts.Shared.Features.Tokens;
using BrowniePts.Shared.Features.Users;
using Xamarin.Forms;

namespace BrowniePts.Mobile.Features.Account
{
    public class SignUpPageViewModel : BaseViewModel
    {
        private readonly ApiService apiService;
        private readonly IKeyValueStorageService storageService;
        private CreateUserCommand createUserCommand;

        public SignUpPageViewModel(ApiService apiService, IKeyValueStorageService storageService, Resolver resolver)
        {
            this.apiService = apiService;
            this.storageService = storageService;
            SubmitNewUserCommand = new Command(SubmitNewUserCommandExecute);
            CreateUserCommand = new CreateUserCommand();
        }   


        public ICommand SubmitNewUserCommand { get; set; }

        public CreateUserCommand CreateUserCommand
        {
            get
            {
                return createUserCommand;
            }

            set
            {
                createUserCommand = value;
                OnPropertyChanged();
            }
        }

        private async void SubmitNewUserCommandExecute()
        {
            Debug.WriteLine(createUserCommand);
            await apiService.MakeRequest<string>("Account/SignUp", createUserCommand, response =>
            {
                if(!string.IsNullOrEmpty(response))
                {
                    storageService.Store(AppSettings.TokenStorageKey, response);
                    Application.Current.MainPage.Navigation.PopModalAsync();
                }
                else
                {
                    //alert user of error
                }
            });
        }
    }
}

