﻿using System;
using System.Collections.Generic;
using BrowniePts.Mobile.Features.Network;
using BrowniePts.Shared.Features;
using BrowniePts.Shared.Features.Users;
using BrowniePts.Shared.Features.Tokens;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;
using System.Diagnostics;
using BrowniePts.Mobile.PlatformServices;
using BrowniePts.Mobile.Configuration;
using BrowniePts.Shared.Features.Resolver;
using System.Threading.Tasks;

namespace BrowniePts.Mobile.Features.Account
{
    public class LoginPageViewModel : BaseViewModel
    {
        private string emailAddress;
        private string password;
        private readonly ApiService apiService;
        private ObservableCollection<UserDto> users;
        private readonly IKeyValueStorageService storageService;
        private readonly Resolver resolver;
        public LoginPageViewModel(ApiService apiService, IKeyValueStorageService storageService, Resolver resolver)
        {
            this.apiService = apiService;
            this.storageService = storageService;
            this.resolver = resolver;
            SubmitLoginCommand = new Command(SubmitLoginCommandExecute);
            NavigateToSignUpCommand = new Command(NavigateToSignUpCommandExecute);
        }

        public string EmailAddress
        {
            get { return emailAddress; }
            set
            {
                emailAddress = value ?? string.Empty;
                OnPropertyChanged();
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                password = value ?? string.Empty;
                OnPropertyChanged();
            }
        }

        public ObservableCollection<UserDto> Users
        {
            get { return users; }
            set
            {
                users = value ?? new ObservableCollection<UserDto>();
                OnPropertyChanged();
            }
        }

        public ICommand SubmitLoginCommand { get; set; }

        public ICommand NavigateToSignUpCommand { get; set; }

        public async void OnAppearing()
        {
            await NavigateBasedOnApiKey();
        }

        private async void SubmitLoginCommandExecute()
        {
            var query = new GetTokenQuery() { EmailAddress = emailAddress, Password = password };
            await apiService.MakeRequest<string>("/Account/SignIn", query, async response =>
            {
                if (response != null)
                {
                    storageService.Store(AppSettings.TokenStorageKey, response);
                    await NavigateBasedOnApiKey();
                }
            });
        }

        private async void NavigateToSignUpCommandExecute()
        {
            var signUpPage = resolver.Resolve<SignUpPage>();
            await Application.Current.MainPage.Navigation.PushModalAsync(signUpPage);
        }

        protected override async Task NavigateBasedOnApiKey()
        {
            var token = storageService.GetByKey(AppSettings.TokenStorageKey);
            if(token != null)
            {
                await Application.Current.MainPage.Navigation.PopModalAsync();
            }
        }
    }
}
