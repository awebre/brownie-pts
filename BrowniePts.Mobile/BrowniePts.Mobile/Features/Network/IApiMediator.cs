﻿using System;
using System.Threading.Tasks;
using BrowniePts.Shared.Commands;
using BrowniePts.Shared.Interfaces;

namespace BrowniePts.Mobile.Features.Network
{
    public interface IApiMediator
    {
        Task Process<TResult>(string endpoint, IQuery<TResult> query, Action<TResult> successAction);
        Task Process(string endpoint, ICommand command, Action successAction);
        Task Process<TResult>(string endpoint, ICommand<TResult> command, Action<TResult> successAction);
    }
}
