﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using BrowniePts.Mobile.Configuration;
using BrowniePts.Mobile.Features.Account;
using BrowniePts.Mobile.Features.Network;
using BrowniePts.Mobile.PlatformServices;
using BrowniePts.Shared.Features.Resolver;
using Xamarin.Forms;

namespace BrowniePts.Mobile.Features.Navigation
{
    public partial class RootPage : ContentPage
    {
        private readonly Resolver resolver;
        private readonly IKeyValueStorageService storageService;
        private readonly ApiService apiService;
        public RootPage(Resolver resolver, IKeyValueStorageService storageService, ApiService apiService)
        {
            this.resolver = resolver;
            this.storageService = storageService;
            this.apiService = apiService;

            NavigateBasedOnKey();
            InitializeComponent();
            var Button = this.FindByName<Button>("button");
            Button.Clicked += async (sender, e) =>
            {
                await apiService.MakeRequest<string>("values/5", null, response =>
                {
                    Debug.WriteLine(response);
                });
            };

            var LogOutButton = this.FindByName<Button>("logout");
            LogOutButton.Clicked += (sender, e) => {
                storageService.Delete(AppSettings.TokenStorageKey);
                NavigateBasedOnKey();
            };
        }

        async void NavigateBasedOnKey(){
            var token = storageService.GetByKey(AppSettings.TokenStorageKey);
            if(token == null){
                var loginPage = resolver.Resolve<LoginPage>();
                await Navigation.PushModalAsync(loginPage);
            }
        }
    }
}
