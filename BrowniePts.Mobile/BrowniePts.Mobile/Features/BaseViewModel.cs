﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using BrowniePts.Mobile.Configuration;
using BrowniePts.Mobile.Features.Account;
using BrowniePts.Mobile.PlatformServices;
using BrowniePts.Shared.Features.Resolver;
using Xamarin.Forms;

namespace BrowniePts.Mobile.Features
{
    public class BaseViewModel : INotifyPropertyChanged
    {


        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null){
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual async Task NavigateBasedOnApiKey()
        {
            var resolver = App.Container.GetInstance<Resolver>();
            var storageService = resolver.Resolve<IKeyValueStorageService>();
            var token = storageService.GetByKey(AppSettings.TokenStorageKey);
            if(token == null)
            {
                var loginPage = resolver.Resolve<LoginPage>();
                await Application.Current.MainPage.Navigation.PushModalAsync(loginPage);    
            }
        }
    }
}
